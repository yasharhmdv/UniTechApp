package com.example.UniTech.models;

import lombok.*;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyRates {

    private LocalDateTime dateTime;

    private Map<String, Float> rates;
}
