package com.example.UniTech.controller;

import com.example.UniTech.dto.request.TransferRequest;
import com.example.UniTech.dto.response.CommonResponse;
import com.example.UniTech.entities.User;
import com.example.UniTech.services.TransferService;
import jakarta.validation.Valid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

import static com.example.UniTech.enums.ResponseEnum.*;

@RestController
@RequestMapping("/api/transfers")
public class TransferController {

    TransferService transferService;

    @PostMapping("/makeTransfer")
    public CommonResponse transfer(@RequestBody @Valid TransferRequest transferRequest) {
        try{
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return transferService.makeTransfer(user.getId(), transferRequest.getFromIdentifier(),
                    transferRequest.getToIdentifier(), transferRequest.getAmount());
        }
        catch (Exception e){
            //LOGGING HERE
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

}
