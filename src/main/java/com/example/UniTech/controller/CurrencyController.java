package com.example.UniTech.controller;

import com.example.UniTech.dto.request.CurrencyRateRequest;
import com.example.UniTech.dto.response.CommonResponse;
import com.example.UniTech.services.CurrencyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

import static com.example.UniTech.enums.ResponseEnum.UNKNOWN_ERROR;

@RestController
@EnableScheduling
@RequiredArgsConstructor
@RequestMapping( "/api/public/currency")
public class CurrencyController {

    private final CurrencyService currencyService;

    @PostMapping("/getRate")
    public CommonResponse getAllAccounts(@RequestBody @Valid CurrencyRateRequest currencyRateRequest) {
        try{
            return currencyService.getRate(currencyRateRequest);
        }
        catch (Exception e){
            //LOG ERROR HERE
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

}
