package com.example.UniTech.controller;

import com.example.UniTech.dto.request.LoginRequest;
import com.example.UniTech.dto.request.RegisterRequest;
import com.example.UniTech.dto.response.CommonResponse;
import com.example.UniTech.dto.response.LoginResponse;
import com.example.UniTech.entities.User;
import com.example.UniTech.repository.AccountRepository;
import com.example.UniTech.repository.UserRepository;
import com.example.UniTech.security.JwtTokenUtil;
import com.example.UniTech.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

import static com.example.UniTech.enums.ResponseEnum.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserRepository userRepository;

    private final UserService userService;

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    private final AccountRepository accountRepository;


    @PostMapping("/public/register")
    public CommonResponse register(@RequestBody @Valid RegisterRequest registerRequest) {
        try{
            return userService.register(registerRequest);
        }
        catch (Exception e){
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

    @PostMapping( "/public/login")
    public CommonResponse login(@RequestBody @Valid LoginRequest request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getPin(), request.getPassword()));

            User user = (User) authenticate.getPrincipal();

            return CommonResponse.success(new LoginResponse(jwtTokenUtil.generateAccessToken(user)));
        }
        catch (BadCredentialsException ex) {
            return new CommonResponse(BAD_CREDENTIALS.getCode(), BAD_CREDENTIALS.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }


    @PostMapping("/self")
    public CommonResponse self() {
        try{
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (user == null) return new CommonResponse(AUTH_ERROR.getCode(), AUTH_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());

            return CommonResponse.success(userRepository.findById(user.getId()));
        }
        catch (Exception e){
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

}
