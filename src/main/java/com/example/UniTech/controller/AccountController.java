package com.example.UniTech.controller;

import com.example.UniTech.dto.request.AddBalanceRequest;
import com.example.UniTech.dto.response.CommonResponse;
import com.example.UniTech.entities.User;
import com.example.UniTech.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

import static com.example.UniTech.enums.ResponseEnum.AUTH_ERROR;
import static com.example.UniTech.enums.ResponseEnum.UNKNOWN_ERROR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/accounts")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/list")
    public CommonResponse getAllAccounts() {
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return accountService.getUserEnabledAccounts(user.getId());
        } catch (Exception e) {
            //LOG ERROR HERE
            System.out.println(e);
            return new CommonResponse(AUTH_ERROR.getCode(), AUTH_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

    @PostMapping("/deactivate")
    public CommonResponse disableAccount(@RequestParam(defaultValue = "0") Long accountId) {
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (user == null)
                return new CommonResponse(AUTH_ERROR.getCode(), AUTH_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());

            return accountService.disableAccount(user.getId(), accountId);
        } catch (Exception e) {
            //LOG ERROR HERE
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

    @PostMapping("/addAccount")
    public CommonResponse addAccount() {
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (user == null)
                return new CommonResponse(AUTH_ERROR.getCode(), AUTH_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());

            return accountService.addAccount(user);
        } catch (Exception e) {
            //LOG ERROR HERE
            System.out.println(e);
            return new CommonResponse(AUTH_ERROR.getCode(), AUTH_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

    @PostMapping( "/addBalance")
    public CommonResponse addBalance(@RequestBody @Validated AddBalanceRequest addBalanceRequest) {
        try {
            return accountService.addBalance(addBalanceRequest.getIdentifier().toUpperCase(), addBalanceRequest.getAmount());
        } catch (Exception e) {
            //LOG ERROR HERE
            System.out.println(e);
            return new CommonResponse(UNKNOWN_ERROR.getCode(), UNKNOWN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());
        }
    }

}
