package com.example.UniTech.services;
import com.example.UniTech.dto.request.RegisterRequest;
import com.example.UniTech.dto.response.CommonResponse;
import com.example.UniTech.entities.User;
import com.example.UniTech.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.UniTech.enums.ResponseEnum.*;
import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public User getByPin(String pin) {
        Optional<User> user = userRepository.findUserByUsername(pin);
        return user.orElse(null);
    }

    public boolean usernameExists(String pin) {
        return userRepository.findUserByUsername(pin).isPresent();
    }

    public List<User> getAll() {
        Iterable<User> usersIterator = userRepository.findAll();
        List<User> users = new ArrayList<>();
        usersIterator.forEach(users::add);

        return users;
    }

    public CommonResponse register(RegisterRequest request) {
        if (userRepository.findUserByUsername(request.getPin()).isPresent())
            return new CommonResponse(SAME_PIN_ERROR.getCode(), SAME_PIN_ERROR.getMessage(), "", new Timestamp(System.currentTimeMillis()).getTime());

        User user = new User();

        user.setFullName(request.getName());
        user.setUsername(request.getPin());
        user.setEnabled(true);

        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));

        userRepository.save(user);

        return CommonResponse.success();
    }

    @Override
    public UserDetails loadUserByUsername(String pin) throws UsernameNotFoundException {
        return userRepository
                .findUserByUsername(pin)
                .orElseThrow(
                        () -> new UsernameNotFoundException(format("User with pin - %s, not found", pin))
                );
    }
}
