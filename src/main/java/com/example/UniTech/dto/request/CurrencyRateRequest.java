package com.example.UniTech.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyRateRequest {
    @NotEmpty
    @NotNull
    private String from;

    @NotEmpty
    @NotNull
    private String to;
}
