package com.example.UniTech.dto.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest {

    @NotNull(message = "Pin required")
    @Size(min = 8, max = 8, message = "Pin must have 7 characters")
    private String pin;

    @NotNull
    @Size(min = 5, message = "Password must have minimum 6 characters")
    private String password;

    @NotNull
    @Size(min = 4, message = "Name must have minimum 3 characters")
    private String name;
}
