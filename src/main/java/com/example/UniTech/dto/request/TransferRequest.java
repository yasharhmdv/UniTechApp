package com.example.UniTech.dto.request;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class TransferRequest {
    @NotNull
    @Size(min = 5)
    private String fromIdentifier;
    @NotNull
    @Size(min = 8)
    private String toIdentifier;
    @NotNull
    private BigDecimal amount;
}
