package com.example.UniTech.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

import static com.example.UniTech.enums.ResponseEnum.SUCCESS;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommonResponse implements Serializable {

    private Integer code;

    private String message;

    private Object data;

    private Long timestamp;

    public static CommonResponse success() {
        return CommonResponse.builder()
                .code(SUCCESS.getCode())
                .message(SUCCESS.getMessage())
                .data(null)
                .timestamp(new Timestamp(System.currentTimeMillis()).getTime())
                .build();
    }

    public static CommonResponse success(Object data) {
        return CommonResponse.builder()
                .code(SUCCESS.getCode())
                .message(SUCCESS.getMessage())
                .data(data)
                .timestamp(new Timestamp(System.currentTimeMillis()).getTime())
                .build();
    }
}
