package com.example.UniTech.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyRatesResponse implements Serializable {

    private String dateTime;

    private String currencies;

    private float rate;
}
